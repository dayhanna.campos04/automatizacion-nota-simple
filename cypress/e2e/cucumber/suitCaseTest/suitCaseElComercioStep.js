import {Given,When,Then,And} from "cypress-cucumber-preprocessor/steps";
import { when } from "cypress/types/jquery";

Given('El usuario ingresa a una nota Simple', () => {
    cy.visit("https://elcomercio.pe/politica/actualidad/renovacion-popular-expulsa-como-militante-a-nuevo-viceministro-de-politicas-para-la-defensa-eduardo-salas-jaen-ministerio-de-defensa-rmmn-noticia/")
})

when('carga la nota simple y se hace un scroll', () => {
    cy.viewport('1382,744')
    cy.scrollTo(0, 500)
})

Then('se verifica que exista el espacio SKIN', () => {
    cy.get('#gpt_skin').verify
})

Then('se verifica que exista el espacio TOP', () => {
    cy.get('#gpt_top').verify
})

Then('se verifica que exista el espacio LATERALL', () => {
    cy.get('#gpt_laterall').verify
})

Then('se verifica que exista el espacio LATERALR', () => {
    cy.get('#gpt_lateralr').verify
})

Then('se verifica que exista el TITULO', () => {
    cy.get('.sht__title').verify
})

Then('se verifica que exista el SUMMARY', () => {
    cy.get('.sht__summary').verify
})

Then('se verifica que exista la IMAGEN ABRIDORA', () => {
    cy.get('.s-multimedia').verify
})

Then('se verifica que exista el espacio CAJA1', () => {
    cy.get('#gpt_caja1').verify
})

Then('se verifica que exista el espacio CAJA2', () => {
    cy.get('#gpt_caja2').verify
})

Then('se verifica que exista el espacio INLINE', () => {
    cy.get('#gpt_inline').verify
})

Then('se verifica que exista el espacio VSLIDER', () => {
    cy.get('#gpt_vslider').verify
})

Then('se verifica que exista el espacio ZOCALO', () => {
    cy.get('#gpt_zocalo').verify
})