Feature: Suit de caso de prueba para nota simple El Comercio

    Verifica espacios publicitarios en Portales - Nota Simple - El Comercio

    Scenario: Verificar una nota Simple de el Portal de El Comercio
        Given El usuario ingresa a una nota Simple
        When carga la nota simple y se hace un scroll
        Then se verifica que exista el espacio SKIN
        Then se verifica que exista el espacio TOP
        Then se verifica que exista el espacio LATERALL
        Then se verifica que exista el espacio LATERALR
        Then se verifica que exista el TITULO
        Then se verifica que exista el SUMMARY
        Then se verifica que exista la IMAGEN ABRIDORA
        Then se verifica que exista el espacio CAJA1
        Then se verifica que exista el espacio CAJA2
        Then se verifica que exista el espacio INLINE
        Then se verifica que exista el espacio VSLIDER
        Then se verifica que exista el espacio ZOCALO