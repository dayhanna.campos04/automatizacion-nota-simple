/// <reference types="Cypress"/>

//Suit de caso de prueba para notas Depor
describe('Verificar espacios publicitarios en Portales - Nota Simple - Depor' ,  function() {
    //Caso de prueba Visualizar Nota simple
    it('Ingresar a una nota', function() {
        cy.visit("https://depor.com/futbol-peruano/descentralizado/universitario-de-deportes-tendra-que-pagar-millonaria-clausula-a-umbro-tras-perder-juicio-con-umbro-empresa-que-vistio-al-club-liga-1-rmmd-noticia/")
    })
    //Caso de prueba verificar que exista el SKIN
    it('Verificar que exista el espacio SKIN', function() {
        cy.get('#gpt_skin').verify
    })
    //Caso de prueba verificar que exista el TOP
    it('Verificar que exista el espacio TOP', function() {
        cy.get('#gpt_top').verify
    })
    //Caso de prueba verificar que exista el LATERALL
    it('Verificar que exista el espacio LATERALL', function() {
        cy.get('#gpt_laterall').verify
    })
    //Caso de prueba verificar que exista el LATERALR
    it('Verificar que exista el espacio LATERALR', function() {
        cy.get('#gpt_lateralr').verify
    })
    //Caso de prueba verificar que exista el TITULO
    it('Verificar que exista el TITULO', function() {
        cy.get('.sht__title').verify
    })
    //Caso de prueba verificar que exista el SUMMARY
    it('Verificar que exista el SUMMARY', function() {
        cy.get('.sht__summary').verify
    })
    //Caso de prueba verificar que exista la IMAGEN ABRIDORA
    it('Verificar que exista la IMAGEN ABRIDORA', function() {
        cy.get('.s-multimedia').verify
    })
    //Caso de prueba verificar que exista la CAJA1
    it('Verificar que exista el espacio CAJA1', function() {
        cy.get('#gpt_caja1').verify
    })
    //Caso de prueba verificar que exista la CAJA2
    it('Verificar que exista el espacio CAJA2', function() {
        cy.get('#gpt_caja2').verify
    })
    //Caso de prueba verificar que exista el INLINE
    it('Verificar que exista el espacio INLINE', function() {
        cy.get('#gpt_inline').verify
    })
    //Caso de prueba verificar que exista el VSLIDER
    it('Verificar que exista el espacio VSLIDER', function() {
        cy.get('#gpt_vslider').verify
    })
    //Caso de prueba verificar que exista el ZOCALO
    it('Verificar que exista el espacio ZOCALO', function() {
        cy.get('#gpt_zocalo').verify
    })
})